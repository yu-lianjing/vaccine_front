import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'
import axios from 'axios'
import VueAxios from 'vue-axios'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'//样式文件
import * as ElIconModules from '@element-plus/icons-vue'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

const app = createApp(App)
for(let iconName in ElIconModules){
    app.component(iconName,ElIconModules[iconName])
}

app.use(ElementPlus, {
    locale: zhCn,
  })

app.use(router)
app.use(ElIconModules)
app.use(VueAxios,axios)

app.mount('#app')

axios.defaults.baseURL = '/api'





