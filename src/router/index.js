import { ElMessage } from 'element-plus'
import { createRouter, createWebHistory } from 'vue-router'
//import Index from '../views/view-user/index.vue'
import Login from '../views/login.vue'

const routes = [
  //首页
  // {
  //   path: '/',       //路径
  //   name: 'Index',
  //   component: Index
  // },
  //登录页
  {
    path: '/login',
    name: 'login',
    component: Login
  },

  //{ path: '/doctor_index', name: '医生首页', component: () => import('../views/view-doctor/doctor_index.vue') },


//404找不到页面
  {
    path: '/:catchAll(.*)',
    name: '404',
    hidden: true,
    meta: {
      name: '404页面'
    },
    //按需加载
    component: () => import('../views/404/404.vue')
  },


  //管理端路由
  {
    path: '/',
    name: 'Manager',
    // meta: {
    //   // meta用于配置自定义数据，login_required为true表示需要登录
    //   require: true,
    // },
    component: () => import('../views/view-manager/Nav.vue'),
    redirect: "/login",
    children: [
      { path: 'carousel', name: '轮播图', component: () => import('../views/view-manager/carousel.vue') },
      { path: 'vaccine', name: '疫苗信息', component: () => import('../views/view-manager/vaccine.vue') },
      { path: 'user', name: '用户管理', component: () => import('../views/view-manager/user.vue') },
      { path: 'evaluate', name: '评论', component: () => import('../views/view-manager/evaluate.vue') },
      { path: 'type', name: '疫苗种类', component: () => import('../views/view-manager/type.vue') },
      { path: 'collect', name: '收藏', component: () => import('../views/view-manager/collect.vue') },
      { path: 'news_manager', name: '新闻', component: () => import('../views/view-manager/news_manager.vue') },
      { path: 'del', name: '黑名单管理', component: () => import('../views/view-manager/del.vue') },
      { path: 'doctorwork', name: '医生排班管理', component: () => import('../views/view-manager/doctorwork.vue') },
      { path: 'health', name: '健康科普管理', component: () => import('../views/view-manager/health.vue') },
    ]
  },

  //医生端路由
  {
    path: '/',
    name: 'Doctor',
    component: () => import('../views/view-doctor/DocNav.vue'),
    redirect: "/login",
    children: [
      { path: '/doctor_index', name: '医生首页', component: () => import('../views/view-doctor/doctor_index.vue') },
      { path: '/doctor_center', name: '医生个人中心', component: () => import('../views/view-doctor/doctor_center.vue') },
      { path: '/DocReply', name: '医生回复咨询', component: () => import('../views/view-doctor/DocReply.vue') },
    ]
  },
  //用户端路由
  {
    path: '/',
    name: 'User',
    component: () => import('../views/view-user/Header.vue'),
    //redirect: "/login", 
    children: [
      { path: '/index', name: 'Index', component: () => import('../views/view-user/index.vue') },
      {
        path: '/me', name: '个人中心', meta: { require: true }, component: () => import('../views/view-user/personal_center.vue'),
        children: [
          { path: '/mycollect', name: '我的收藏', meta: { require: true }, component: () => import('../views/view-user/mycollect.vue') },
          { path: '/myinformation', name: '我的信息', meta: { require: true }, component: () => import('../views/view-user/myinformation.vue') },
          { path: '/myquestion', name: '我的留言', meta: { require: true }, component: () => import('../views/view-user/myquestion.vue') },
        ]
      },
      { path: '/vaccine_user', name: '疫苗列表', component: () => import('../views/view-user/vaccine_user.vue') },
      { path: '/vaccineDetails/:id', name: '疫苗详情', meta: { require: true }, component: () => import('../views/view-user/vaccineDetails.vue') },
      { path: '/booking', name: '我的预约', component: () => import('../views/view-user/booking.vue') },
      { path: '/news', name: '新闻资讯', component: () => import('../views/view-user/news.vue') },
      { path: '/recording', name: '接种记录', component: () => import('../views/view-user/recording.vue') },
      { path: '/knowledge', name: '健康知识', component: () => import('../views/view-user/knowledge.vue') },
    ]
  },

]

const router = createRouter({
  history: createWebHistory(),//这个一定要加上
  routes
})

//路由拦截
router.beforeEach(function (to, from, next) {
  //logged_in变量 记录用户是否登录
  var logged_in = false
  var logged = window.localStorage.getItem('uname') //取出的是string类型
  //var logged2 = parseInt(window.localStorage.getItem('userid')); //取出的是int类型

  if (logged !== '') {
    logged_in = true
  }
  if (!logged_in && to.matched.some(function (item) {
    return item.meta.require
  })) {
    ElMessage({
      message: '您还未登录，请登录！',
      type: 'error'
    })
    next('/login');
  } else {
    next();
  }
});

export default router